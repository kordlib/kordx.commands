#0.1.1

## Fixes

* Fixed an unescaped regex when enabling `enableMentionPrefix`
